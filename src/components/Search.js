/* eslint-disable react/prop-types */
import React from 'react'
import '../styles/Search.css'
function Search({ filter, handleInputChange }) {
  return (
    <nav>
      <form>
        <input
          type='text'
          name='filter'
          value={filter}
          onChange={(event) => handleInputChange(event)}
        />
      </form>
    </nav>
  )
}

export default Search
