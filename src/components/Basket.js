import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { STRIKE_OFF } from '../redux/grocerySlice'
import '../styles/basket.css'
// import { REMOVE_ITEM } from '../redux/grocerySlice'

function Basket() {
  const dispatch = useDispatch()
  const items = useSelector((state) => state.cart)
  const onCartItemClick = (e) => {
    console.log(e.target.innerText)
    dispatch(STRIKE_OFF(e.target.innerText))
  }
  return (
    <div>
      <ul>
        {items && items.length !== 0 ? (
          items.map((item, index) => (
            <li
              className={`basket_list ${item.isCompleted ? 'completed' : ''}`}
              key={index}
              onClick={(e) => onCartItemClick(e)}
            >
              <span
                className={`basket_icon ${item.isCompleted ? 'completed' : ''}`}
              >
                <i className='far fa-minus-square'></i>
              </span>
              <span
                className={`basket_quantity ${
                  item.isCompleted ? 'completed' : ''
                }`}
              >
                {item.quantity}
              </span>
              <span
                className={`basket_item ${item.isCompleted ? 'completed' : ''}`}
              >
                {item.itemName}
              </span>
            </li>
          ))
        ) : (
          <div>Your Cart is Empty</div>
        )}
      </ul>
    </div>
  )
}

export default Basket
