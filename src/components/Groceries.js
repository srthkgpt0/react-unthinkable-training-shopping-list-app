/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { groceries } from '../groceries'
import { ADD_ITEM } from '../redux/grocerySlice'
import PropTypes from 'prop-types';
import '../styles/groceries.css'
function Groceries({ filter }) {
  const [items, setItems] = useState(groceries)
  useEffect(() => {
    setItems(
      groceries.filter((item) =>
        item.name.toLowerCase().includes(filter.toLowerCase())
      )
    )
  }, [filter])
  const dispatch = useDispatch()
  const handleGroceriesClick = (e) => {
    dispatch(ADD_ITEM(e.target.innerText))
  }
  return (
    <div>
      <ul onClick={(e) => handleGroceriesClick(e)}>
        {items.map((item, index) => (
          <li
            className='grocery_list'
            style={{ textDecoration: 'none' }}
            key={index}
          >
            <span className='plus_icon'>
              <i className='far fa-plus-square'></i>
            </span>
            {item.name}
          </li>
        ))}
      </ul>
    </div>
  )
}
Groceries.propTypes = {
  filter:PropTypes.string
}
export default Groceries
