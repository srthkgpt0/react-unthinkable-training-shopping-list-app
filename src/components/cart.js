import React from 'react'
import Groceries from './Groceries'
import Basket from './Basket'
import '../styles/cart.css'
import { useDispatch } from 'react-redux'
import { EMPTY_CART } from '../redux/grocerySlice'
import PropTypes from 'prop-types';

function Cart({ filter }) {
  const dispatch = useDispatch()

  const handleClearCart = () => {
    dispatch(EMPTY_CART())
  }
  return (
    <main className='main-content'>
      <div className='groceries'>
        <header className='grocery'>
          <h3>
            <i className='fas fa-leaf'></i>
            Groceries
          </h3>
        </header>
        <Groceries filter={filter} />
      </div>
      <div className='cart'>
        <header className='cart_header'>
          <h3>
            <i className='fas fa-shopping-basket'></i>
            Basket
          </h3>
          <h3 onClick={() => handleClearCart()}>
            <i className='far fa-trash-alt'></i>
          </h3>
        </header>
        <Basket />
      </div>
    </main>
  )
}
Cart.propTypes = {
  filter:PropTypes.string
}

export default Cart
