import React from 'react'
import '../styles/header.css'
function Header() {
  return (
    <header className='App-header'>
      <div className='App-icon'>
        <i className='fas fa-shopping-basket'></i>
      </div>
      <h1 className='title'>Hello, Basket!</h1>
    </header>
  )
}

export default Header
