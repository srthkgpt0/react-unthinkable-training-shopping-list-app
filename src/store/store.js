import { configureStore } from '@reduxjs/toolkit'
import cartReducer from '../redux/grocerySlice'
export default configureStore({
  reducer: { cart: cartReducer }
})
