export const groceries = [
  {
    name: 'strawberry'
  },
  {
    name: 'blueberry'
  },
  {
    name: 'orange'
  },
  {
    name: 'banana'
  },
  {
    name: 'apple'
  },
  { name: 'carrot' },
  { name: 'celery' },
  { name: 'mushroom' },
  { name: 'green pepper' },
  { name: 'eggs' },
  { name: 'cheeze' },
  { name: 'butter' },
  { name: 'chicken' },
  { name: 'beef' },
  { name: 'pork' },
  { name: 'fish' },
  { name: 'rice' },
  { name: 'pasta' },
  { name: 'bread' }
]
