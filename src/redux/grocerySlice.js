import { createSlice } from '@reduxjs/toolkit'

export const cartSlice = createSlice({
  name: 'cart',
  initialState: [
    {
      itemName: 'strawberry',
      isCompleted: false,
      quantity: 1
    },
    {
      itemName: 'apple',
      isCompleted: true,
      quantity: 3
    }
  ],
  reducers: {
    ADD_ITEM: (state, action) => {
      const item = state.find(
        (b) => action.payload.toLowerCase() === b.itemName.toLowerCase()
      )
      const items = state.filter(
        (b) => action.payload.toLowerCase() !== b.itemName.toLowerCase()
      )
      if (item) {
        return [
          ...items,
          {
            ...item,
            quantity: item.quantity + 1
          }
        ]
      } else {
        return [
          ...state,
          { itemName: action.payload, isCompleted: false, quantity: 1 }
        ]
      }
    }, 
    EMPTY_CART: () => {
      return []
    },
    STRIKE_OFF: (state, action) => {
      const item = state.find(
        (b) => action.payload.toLowerCase() === b.itemName.toLowerCase()
      )
      const items = state.filter(
        (b) => action.payload.toLowerCase() !== b.itemName.toLowerCase()
      )
      if (item) {
        return [
          ...items,
          {
            ...item,
            isCompleted: !item.isCompleted
          }
        ]
      }
    }
  }
})
export const { ADD_ITEM, EMPTY_CART, STRIKE_OFF } = cartSlice.actions
export default cartSlice.reducer

