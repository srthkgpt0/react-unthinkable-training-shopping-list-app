import React from 'react'
import Header from './components/header'
import Search from './components/Search'
import Cart from './components/cart'
import Footer from './components/Footer'
import { useState } from 'react'

function App() {
  const [filter, setFilter] = useState('')
  const handleInputChange = (e) => {
    setFilter(e.target.value)
  }
  return (
    <div className='App'>
      <Header />
      <Search filter={filter} handleInputChange={handleInputChange} />
      <Cart filter={filter} />
      <Footer />
    </div>
  )
}

export default App
